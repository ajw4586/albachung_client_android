package kr.albachung.stac;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import kr.albachung.stac.util.Logger;

/**
 * A placeholder fragment containing a simple view.
 */
public class AlbaMainWebViewFragment extends CommonFragment {

    public static final int RES_ID = R.layout.fragment_alba_main_web_view;


    private WebView webView;
    private AlbachungWebClient webClient;

    private String url;

    public AlbaMainWebViewFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(RES_ID, container, false);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    protected void init() {
        url = ((AlbaMainWebView) getActivity()).getCurrentUrl();
    }

    @Override
    protected void initView() {
        webView = (WebView) rootView.findViewById(R.id.web);
        if(url.equals(""))
            webView.loadUrl("http://175.158.15.181/");
        else {
            webView.loadUrl(url);
            Logger.single(getClass(), "requested site is loading.");
        }
        webView.setWebViewClient(new AlbachungWebClient());
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void viewUrl(String url) {
        webView.loadUrl(url);
    }


    public class AlbachungWebClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    public class AlbachungChromeClient extends WebChromeClient {

    }
}

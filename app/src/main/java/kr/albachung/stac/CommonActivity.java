package kr.albachung.stac;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Albachung base Activity.
 * This class contains usually using development patternand common constant.
 * Created by Ahn Jae Wook on 2015. 8. 9..
 */
public abstract class CommonActivity extends AppCompatActivity {

    private int layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(layout == 0)
            throw new RuntimeException("You MUST set activity's layout resource id.");
        super.onCreate(savedInstanceState);
        init();
        setContentView(layout);
        initView();
        initMenu();
    }

    protected void setLayoutRes(int res) {
        layout = res;
    }

    /**
     * Overall Activity Initialize routine.
     * I recommend you to init fragment management
     * or prepare network connect.
     */
    protected abstract void init();

    /**
     * Initialize Views that show on this activity.
     * Add listeners, do default task in this callback.
     */
    protected abstract void initView();

    /**
     * If you face to use menu component,
     * preapre menu in this callback.
     */
    protected abstract void initMenu();
}

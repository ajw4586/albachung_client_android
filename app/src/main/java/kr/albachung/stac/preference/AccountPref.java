package kr.albachung.stac.preference;

import android.content.Context;
import android.content.SharedPreferences;

import kr.albachung.stac.network.response.AccountResponse;
import kr.albachung.stac.util.Logger;

/**
 * Created by globalsmartkr on 2015. 8. 27..
 */
public class AccountPref {

    public static final String ACCOUNT_PREF = "account_pref";
    //  FB
    public static final String FB_ACCESS_TOKEN = "access_token";
    public static final String USER_ID = "user_id";
    //  ALBACHUNG
    public static final String APP_ACCESS_TOKEN = "app_access_token";
    public static final String EXPIRES_AT = "expires_at";
    public static final String USERNAME = "username";
    public static final String PROFILE_IMAGE = "profile_img";

    public static void saveFacebookUserData(Context context, String access_token, String user_id) {
        SharedPreferences preferences = context.getSharedPreferences(ACCOUNT_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(FB_ACCESS_TOKEN, access_token);
        editor.putString(USER_ID, user_id);
        editor.commit();
        Logger.bold(AccountPref.class, "Succeeded to save fb user data.");
    }

    public static String getFbUserToken(Context context) {
        return context.getSharedPreferences(ACCOUNT_PREF, Context.MODE_PRIVATE)
                .getString(FB_ACCESS_TOKEN, null);
    }

    public static void saveAppUserInfo(Context context, AccountResponse account) {
        SharedPreferences preferences = context.getSharedPreferences(ACCOUNT_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(EXPIRES_AT, account.expires_at);
        editor.putString(APP_ACCESS_TOKEN, account.access_token);
        editor.putString(PROFILE_IMAGE, account.user.profile_img);
        editor.putString(USERNAME, account.user.username);
        editor.commit();
    }

    public static String getAppToken(Context context) {
        return context.getSharedPreferences(ACCOUNT_PREF, Context.MODE_PRIVATE)
                .getString(APP_ACCESS_TOKEN, null);
    }

    public static String getProfileImgUrl(Context context) {
        return context.getSharedPreferences(ACCOUNT_PREF, Context.MODE_PRIVATE)
                .getString(PROFILE_IMAGE, null);
    }

    public static String getUserName(Context context) {
        return context.getSharedPreferences(ACCOUNT_PREF, Context.MODE_PRIVATE)
                .getString(USERNAME, null);
    }

    public static void deleteInfo(Context context) {
        SharedPreferences pref = context.getSharedPreferences(ACCOUNT_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = pref.edit();
        edit.remove(APP_ACCESS_TOKEN);
        edit.remove(FB_ACCESS_TOKEN);
        edit.remove(EXPIRES_AT);
        edit.remove(USER_ID);
        edit.remove(PROFILE_IMAGE);
        edit.remove(USERNAME);
        edit.commit();
    }

    public static boolean isLogined(Context context) {
        SharedPreferences pref = context.getSharedPreferences(ACCOUNT_PREF, Context.MODE_PRIVATE);
        return !pref.getString(APP_ACCESS_TOKEN, "").equals("");
    }


}

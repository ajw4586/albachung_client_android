package kr.albachung.stac;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.facebook.login.LoginManager;

import de.hdodenhof.circleimageview.CircleImageView;
import kr.albachung.stac.network.DefaultImageLoader;
import kr.albachung.stac.preference.AccountPref;


public class AlbaMain extends CommonActivity implements SearchView.OnQueryTextListener {

    //  Menu
    private DrawerLayout menuDrawer;
    private ActionBarDrawerToggle drawerToggle;
    private NavigationView naView;

    //  Menu Header view
    private CircleImageView profileImage;
    private TextView profileName;

    //  Volley Image Loader
    private ImageLoader imageLoader;

    private Toolbar toolbar;
    private SearchView mSearchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setLayoutRes(R.layout.activity_alba_main);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setBackgroundColor(Color.TRANSPARENT);

        naView = (NavigationView) findViewById(R.id.alba_main_nav_view);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_alba_main, menu);

        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem searchViewMenuItem = menu.findItem(R.id.alba_main_action_search);
        mSearchView = (SearchView) searchViewMenuItem.getActionView();
        mSearchView.setOnQueryTextListener(this);
        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }


    protected void init() {
        //  Init imageLoader
        imageLoader = DefaultImageLoader.getInstance(this).getImageLoader();
    }


    protected void initMenu() {
        menuDrawer = (DrawerLayout) findViewById(R.id.alba_main_drawer);
        drawerToggle = new ActionBarDrawerToggle(this, menuDrawer, toolbar, R.string.blank, R.string.blank);
        menuDrawer.setDrawerListener(drawerToggle);

        //  Init menu header views.
        profileImage = (CircleImageView) findViewById(R.id.drawer_profile_image);
        profileName = (TextView) findViewById(R.id.drawer_profile_name);

        profileName.setText(AccountPref.getUserName(this));
        //  Get profile image
        imageLoader.get(AccountPref.getProfileImgUrl(this), new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                profileImage.setImageBitmap(response.getBitmap());
            }

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        naView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                switch(menuItem.getItemId()) {
                    case R.id.menu_main_logout:
                        LoginManager.getInstance().logOut();
                        AccountPref.deleteInfo(AlbaMain.this);
                        startActivity(new Intent(AlbaMain.this, AlbaLogin.class));
                        finish();
                        break;
                    default:
                        break;
                }
                return true;
            }
        });
    }
}

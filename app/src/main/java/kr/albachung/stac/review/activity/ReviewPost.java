package kr.albachung.stac.review.activity;

import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import kr.albachung.stac.CommonActivity;
import kr.albachung.stac.CommonFragment;
import kr.albachung.stac.R;
import kr.albachung.stac.network.request.AlbaService;
import kr.albachung.stac.network.response.BaseResponse;
import kr.albachung.stac.preference.AccountPref;
import kr.albachung.stac.review.fragment.ReviewCommentFragment;
import kr.albachung.stac.review.fragment.ReviewEvaluateFragment;
import kr.albachung.stac.review.fragment.ReviewPlacePickerFragment;
import kr.albachung.stac.review.fragment.ReviewTagFragment;
import kr.albachung.stac.util.Logger;
import kr.albachung.stac.util.Utils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class ReviewPost extends CommonActivity {

    public static final int REVIEW_FRAGMENT = R.id.review_fragment;

    private Toolbar toolbar;

    private FragmentManager mFManager;
    private FragmentTransaction mFTransaction;

    //  Review Fragments.
    public ReviewPlacePickerFragment reviewPlacePicker;
    public ReviewEvaluateFragment reviewEvaluate;
    public ReviewTagFragment reviewTag;
    public ReviewCommentFragment reviewComment;

    //  Request POST Alba review.
    private AlbaService.AlbaInterface albaInterface = AlbaService.getInstance();


    //  alba review information objects.
    private String place_name, place_address, place_id, comment;
    private double place_lat, place_lng;
    private int[] scores;
    private String[] tag_array;

    public FragmentMover fragmentMover = new FragmentMover() {
        @Override
        public void move(CommonFragment original, CommonFragment current) {
            mFTransaction = mFManager.beginTransaction();
            mFTransaction.replace(REVIEW_FRAGMENT, current)
                    .addToBackStack(null)
                    .commit();
            Logger.single(getClass(), "Fragment moved from " + original.getClass().getSimpleName() +
                    " to " + current.getClass().getSimpleName());
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setLayoutRes(R.layout.activity_review_post);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void init() {
        //  Get Fragment Management instances.
        mFManager = getSupportFragmentManager();
        mFTransaction = mFManager.beginTransaction();

        //  Create Each Fragments.
        reviewPlacePicker = new ReviewPlacePickerFragment();
        reviewEvaluate = new ReviewEvaluateFragment();
        reviewTag = new ReviewTagFragment();
        reviewComment = new ReviewCommentFragment();
    }

    @Override
    protected void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //  Attach Fragment to activity.
        mFTransaction.replace(REVIEW_FRAGMENT, reviewPlacePicker)
                .commit();
    }

    @Override
    protected void initMenu() {
        //  This activity does not have any menu.
    }

    public void setAlbaInfo(String id, String name, String address, double lat, double lng) {
        place_id = id;
        place_name = name;
        place_address = address;
        place_lat = lat;
        place_lng = lng;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setReviewData(int[] scores) {
        this.scores = scores;
    }

    public void setTagArray(String tags) {
        //  Remove blank space.
        tags.replace(" ", "");
        //  Split by hash symbol.
        tag_array = tags.split("#");
    }


    public void postAlbaReview() {
        albaInterface.postAlbaReview(buildRequest(AccountPref.getAppToken(ReviewPost.this),
                buildReviewJSON(), buildAlbaJSON(), comment, buildTagArray()),
                new Callback<BaseResponse>() {
                    @Override
                    public void success(BaseResponse baseResponse, Response response) {
                        if(response.getStatus() == 200) {
                            finish();
                            Toast.makeText(ReviewPost.this, "감사합니다! 소중한 리뷰가 등록되었습니다.", Toast.LENGTH_SHORT).show();
                        } else
                            Logger.except(getClass(), response.toString());
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Logger.except(getClass(), error.toString());
                    }
                });
    }

    public JsonObject buildReviewJSON() {
        Utils.JSONBuilder builder = new Utils.JSONBuilder();
        return builder.add("boss", scores[0])
                .add("money", scores[1])
                .add("work", scores[2])
                .add("client" ,scores[3])
                .add("partner", scores[4])
                .buildObject();
    }

    public JsonObject buildAlbaJSON() {
        Utils.JSONBuilder builder = new Utils.JSONBuilder();
        return builder.add("name", place_name)
                .add("lat", place_lat)
                .add("lng", place_lng)
                .add("address", place_address)
                .add("place_id", place_id)
                .buildObject();
    }

    public JSONArray buildTagArray() {
        Utils.JSONBuilder builder = new Utils.JSONBuilder(Utils.JSONBuilder.Type.ARRAY);
        for(String tag : tag_array)
            builder.append(tag);
        return builder.buildArray();
    }

    public JSONObject buildRequest(String token, JsonObject review, JsonObject alba, String comment, JSONArray tags) {
        JSONObject request = new JSONObject();
        try {
            request.put("token", token);
            request.put("review", review);
            request.put("alba", alba);
            request.put("comment", comment);
            request.put("tag_array", tags);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return request;
    }


    public interface FragmentMover {
        void move(CommonFragment original, CommonFragment current);
    }
}

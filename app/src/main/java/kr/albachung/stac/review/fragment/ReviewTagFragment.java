package kr.albachung.stac.review.fragment;


import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import kr.albachung.stac.CommonFragment;
import kr.albachung.stac.R;
import kr.albachung.stac.review.activity.ReviewPost;


/**
 * A simple {@link Fragment} subclass.
 */
public class ReviewTagFragment extends CommonFragment implements View.OnClickListener {


    //  Layout XML Resource ID.
    protected static final int RES_ID = R.layout.fragment_review_tag;

    private ReviewPost parent;

    private Button submit;
    private EditText field;

    public ReviewTagFragment() {
        // Required empty public constructor
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        parent = (ReviewPost) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        parent = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(RES_ID, container, false);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    protected void init() {
    }

    @Override
    protected void initView() {
        submit = (Button) rootView.findViewById(R.id.review_tag_submit);
        field = (EditText) rootView.findViewById(R.id.review_tag_field);
        submit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.review_tag_submit:
                parent.setTagArray(field.getText().toString());
                //  Submit the alba review.
                parent.postAlbaReview();
                break;
        }
    }
}

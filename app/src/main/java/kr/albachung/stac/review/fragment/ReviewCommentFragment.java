package kr.albachung.stac.review.fragment;


import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import kr.albachung.stac.CommonFragment;
import kr.albachung.stac.R;
import kr.albachung.stac.review.activity.ReviewPost;


/**
 * A simple {@link Fragment} subclass.
 */
public class ReviewCommentFragment extends CommonFragment implements View.OnClickListener {


    protected static final int RES_ID = R.layout.fragment_review_comment;

    private ReviewPost parent;

    private EditText field;
    private Button back, next;
    private CheckBox anonymous;

    public ReviewCommentFragment() {
        // Required empty public constructor
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        parent = (ReviewPost) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        parent = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(RES_ID, container, false);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    protected void init() {

    }

    @Override
    protected void initView() {
        field = (EditText) rootView.findViewById(R.id.review_comment_field);
        anonymous = (CheckBox) rootView.findViewById(R.id.review_comment_anonymous_check);
        back = (Button) rootView.findViewById(R.id.review_comment_back);
        next = (Button) rootView.findViewById(R.id.review_comment_next);
        back.setOnClickListener(this);
        next.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.review_comment_back:
                parent.fragmentMover.move(ReviewCommentFragment.this, parent.reviewEvaluate);
                //  Remove fragment backstack
                parent.getFragmentManager().popBackStack();
                parent.getFragmentManager().popBackStack();
                break;
            case R.id.review_comment_next:
                parent.setComment(field.getText().toString());
                parent.fragmentMover.move(ReviewCommentFragment.this, parent.reviewTag);
                break;
        }
    }
}

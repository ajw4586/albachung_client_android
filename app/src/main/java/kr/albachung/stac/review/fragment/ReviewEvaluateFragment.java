package kr.albachung.stac.review.fragment;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import kr.albachung.stac.CommonFragment;
import kr.albachung.stac.R;
import kr.albachung.stac.preference.AccountPref;
import kr.albachung.stac.review.activity.ReviewPost;
import kr.albachung.stac.view.SliderView;


/**
 * A simple {@link Fragment} subclass.
 */
public class ReviewEvaluateFragment extends CommonFragment implements View.OnClickListener {


    protected static final int RES_ID = R.layout.fragment_review_evaluate;

    private ReviewPost parent;

    private SliderView[] slider = new SliderView[5];
    private int[] slider_ids = {
            R.id.review_evaluate_q1,
            R.id.review_evaluate_q2,
            R.id.review_evaluate_q3,
            R.id.review_evaluate_q4,
            R.id.review_evaluate_q5
    };
    private Button back, next;
    private TextView message;

    public ReviewEvaluateFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        parent = (ReviewPost) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        parent = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(RES_ID, container, false);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    /**
     * Handle when pressing back key.
     * fragment backstack is not required.
     * it just move between previous fragment without losing data.
     * (backstack is undo the transaction, so this will losing current data.
     * purpose to save the current data.
     */
    public void setBackKeyEvent() {
        //  TODO: 요청사항 들어올때 작업
    }

    @Override
    protected void init() {

    }

    @Override
    protected void initView() {
        for(int i = 0; i < 5; i++)
            slider[i] = (SliderView) rootView.findViewById(slider_ids[i]);
        back = (Button) rootView.findViewById(R.id.review_evaluate_back);
        next = (Button) rootView.findViewById(R.id.review_evaluate_next);
        message = (TextView) rootView.findViewById(R.id.review_evaluate_name);
        back.setOnClickListener(this);
        next.setOnClickListener(this);
        message.setText(AccountPref.getUserName(getActivity()) + "님의 리뷰가 큰 도움이 될거에요!");
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.review_evaluate_back:
                parent.fragmentMover.move(ReviewEvaluateFragment.this, parent.reviewPlacePicker);
                //  TODO: back key event를 받아서 handle하게 되면 제거.
                //  Remove fragment transaction history
                parent.getFragmentManager().popBackStack();
                parent.getFragmentManager().popBackStack();
                break;
            case R.id.review_evaluate_next:
                int[] scores = new int[5];
                for(int i = 0; i < 5; i++) {
                    scores[i] = slider[i].getCurLevel() + 1;
                }
                parent.setReviewData(scores);

                parent.fragmentMover.move(ReviewEvaluateFragment.this, parent.reviewComment);
                break;
        }
    }

}

package kr.albachung.stac.review.fragment;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import kr.albachung.stac.CommonFragment;
import kr.albachung.stac.R;
import kr.albachung.stac.adapter.AlbaDetailCommentsAdapter;


/**
 * A placeholder fragment containing a simple view.
 */
public class AlbaPlaceDetaillFragment extends CommonFragment {


    protected static final int RES_ID = R.layout.fragment_alba_place_detaill;

    private ListView commentsList;
    private AlbaDetailCommentsAdapter commentsAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        commentsAdapter = new AlbaDetailCommentsAdapter(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(RES_ID, container, false);
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    @Override
    protected void init() {

    }

    @Override
    protected void initView() {
        commentsList = (ListView) rootView.findViewById(R.id.alba_place_detail_comments_list);
        commentsList.setAdapter(commentsAdapter);
    }
}

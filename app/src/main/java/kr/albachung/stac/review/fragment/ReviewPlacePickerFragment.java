package kr.albachung.stac.review.fragment;

import android.app.Activity;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import kr.albachung.stac.CommonFragment;
import kr.albachung.stac.R;
import kr.albachung.stac.adapter.AlbaSearchResultAdapter;
import kr.albachung.stac.location.GPSManager;
import kr.albachung.stac.network.request.AlbaService;
import kr.albachung.stac.network.request.GoogleService;
import kr.albachung.stac.network.response.GooglePlaceSearchResponse;
import kr.albachung.stac.review.activity.ReviewPost;
import kr.albachung.stac.util.Logger;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * A placeholder fragment containing a simple view.
 */
public class ReviewPlacePickerFragment extends CommonFragment
        implements View.OnClickListener, GPSManager.LocationResponse {


    //  Layout XML Resource ID.
    protected static final int RES_ID = R.layout.fragment_review_place_picker;

    private ReviewPost parent;

    private GPSManager gpsManager;

    private RecyclerView placeList;
    private LinearLayoutManager listManager;

    private EditText search_field;
    private Button search_button, next;

    private AlbaSearchResultAdapter adapter;


    private String latlng;

    //  Google Map Search
    private GoogleService.GoogleInterface googleInterface = GoogleService.getInstance();

    public ReviewPlacePickerFragment() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        parent = (ReviewPost) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        parent = null;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(RES_ID, container, false);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    protected void init() {
        gpsManager = GPSManager.getInstance(getActivity());
        gpsManager.setLocationResponse(this);
        //  Start getting location.
        gpsManager.getLocation();
        adapter = new AlbaSearchResultAdapter();
    }

    @Override
    protected void initView() {
        placeList = (RecyclerView) rootView.findViewById(R.id.review_place_picker_list);
        listManager = new LinearLayoutManager(getActivity());
        placeList.setLayoutManager(listManager);
        placeList.setAdapter(adapter);

        search_field = (EditText) rootView.findViewById(R.id.review_place_picker_search_field);
        search_button = (Button) rootView.findViewById(R.id.review_place_picker_search_btn);
        next = (Button) rootView.findViewById(R.id.review_place_picker_next);

        search_button.setOnClickListener(this);
        next.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.review_place_picker_search_btn:
                if(latlng != null) {
                    googleInterface.searchByName(latlng, search_field.getText().toString(), 3000, new Callback<GooglePlaceSearchResponse>() {
                        @Override
                        public void success(GooglePlaceSearchResponse googlePlaceSearchResponse, Response response) {
                            Logger.bold(getClass(), "Status: " + googlePlaceSearchResponse.status);
                            if(googlePlaceSearchResponse.status.equals("OK")) {
                                Logger.bold(getClass(), "Succeeded Google Place Search");
                                adapter.updateData(googlePlaceSearchResponse);
                            } else if(googlePlaceSearchResponse.status.equals("ZERO_RESULTS")) {
                                Logger.single(getClass(), "Any result has been found.");
                                adapter.updateData(googlePlaceSearchResponse);
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Toast.makeText(getActivity(), "죄송합니다. 검색 도중 문제가 발생하였습니다.\n" + error.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    Toast.makeText(getActivity(), "위치를 확인할 수 없습니다.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.review_place_picker_next:
                if(adapter.getSelectedItem() == null) {
                    Toast.makeText(getActivity(), "먼저 아르바이트 하신 곳을 선택해주세요!", Toast.LENGTH_SHORT).show();
                    break;
                }

                //  Set the alba info to parent alba info collecting module.
                parent.setAlbaInfo(adapter.getSelectedItem().place_id,
                        adapter.getSelectedItem().name,
                        adapter.getSelectedItem().vicinity,
                        adapter.getSelectedItem().geometry.location.lat,
                        adapter.getSelectedItem().geometry.location.lng);

                //  Change the fragment to next step.
                ((ReviewPost) getActivity()).fragmentMover
                        .move(this, ((ReviewPost) getCommonActivity()).reviewEvaluate);
                break;
        }
    }



    @Override
    public void onReceive(Location location) {
        Logger.multi("위치 검색됨.", "위도: " + location.getLatitude(), "경도: " + location.getLongitude());
        latlng = location.getLatitude() + "," + location.getLongitude();
    }
}

package kr.albachung.stac;


import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import kr.albachung.stac.adapter.AlbaPlaceAdapter;


/**
 * A simple {@link Fragment} subclass.
 */
public class AlbaPlaceListFragment extends CommonFragment {


    protected static final int RES_ID = R.layout.fragment_alba_place_list;

    private RecyclerView albaList;
    private AlbaPlaceAdapter adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(RES_ID, container, false);
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    @Override
    protected void init() {
        adapter = new AlbaPlaceAdapter();
    }

    @Override
    protected void initView() {
        albaList = (RecyclerView) rootView.findViewById(R.id.alba_place_list);
        LinearLayoutManager liLayoutMgr = new LinearLayoutManager(getActivity());
        liLayoutMgr.setOrientation(LinearLayoutManager.VERTICAL);
        albaList.setAdapter(adapter);
        albaList.setLayoutManager(liLayoutMgr);
    }


}
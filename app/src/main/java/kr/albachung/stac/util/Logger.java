package kr.albachung.stac.util;

import android.util.Log;

/**
 * Created by globalsmartkr on 2015. 8. 11..
 */
public class Logger {

    public static final String TAG = "Albachung Logger";

    public static void multi(String...texts) {
        StringBuilder sb = new StringBuilder();
        for(String s : texts)
            sb.append(s + "\n");
        Log.d(TAG, sb.toString());
    }

    public static void bold(Class name, String text) {
        Log.w(name.getSimpleName(), text);
    }

    public static void single(Class name, String text) {
        Log.d(name.getSimpleName(), text);
    }

    public static void except(Class name, String error) {
        Log.e(name.getSimpleName(), error);
    }
}

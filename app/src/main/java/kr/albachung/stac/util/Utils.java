package kr.albachung.stac.util;

import android.content.Context;
import android.util.TypedValue;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by globalsmartkr on 2015. 9. 7..
 */
public class Utils {

    public static int dpToPx(Context context, int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }

    public static class JSONBuilder {
        private JsonObject json;
        private JSONArray array;

        public enum Type {
            OBJECT, ARRAY
        }

        public JSONBuilder() {
            json = new JsonObject();
        }

        public JSONBuilder(Type type) {
            if(type.equals(Type.OBJECT))
                json = new JsonObject();
            else if(type.equals(Type.ARRAY))
                array = new JSONArray();
            else
                json = new JsonObject();
        }

        public JSONBuilder add(String key, String value) {
            json.addProperty(key, value);
            return this;
        }

        public JSONBuilder add(String key, Number value) {
            json.addProperty(key, value);
            return this;
        }

        public JSONBuilder add(String key, boolean value) {
            json.addProperty(key, value);
            return this;
        }

        public JSONBuilder append(String value) {
            array.put(value);
            return this;
        }

        public JSONBuilder append(int value) {
            array.put(value);
            return this;
        }

        public JsonObject buildObject() {
            return json;
        }

        public JSONArray buildArray() {
            return array;
        }
    }
}

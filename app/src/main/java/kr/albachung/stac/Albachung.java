package kr.albachung.stac;

import android.app.Application;
import android.content.Intent;

import kr.albachung.stac.preference.AccountPref;

/**
 * Check user is already logined and move the activity as the result.
 *
 * Created by globalsmartkr on 2015. 9. 8..
 */
public class Albachung extends Application {

    public Albachung() {
        super();
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

}

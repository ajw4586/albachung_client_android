package kr.albachung.stac;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Common abstract class declared logical abstract method.
 * You can easily develop fragment logic.
 *
 * Created by globalsmartkr on 2015. 8. 16..
 */
public abstract class CommonFragment extends Fragment {

    protected View rootView;

    /**
     * You must override this method to inflate rootView View object from XML.
     * If you don't override this, rootView will return null and occur NullPointerException.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        init();
        initView();
        return rootView;
    }

    protected abstract void init();
    protected abstract void initView();


    public final CommonActivity getCommonActivity() {
        return (CommonActivity) getActivity();
    }
}

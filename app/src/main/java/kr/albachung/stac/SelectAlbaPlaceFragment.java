package kr.albachung.stac;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A placeholder fragment containing a simple view.
 */
public class SelectAlbaPlaceFragment extends CommonFragment {


    protected static final int RES_ID = R.layout.fragment_select_alba_plcae;

    private TextView place, location;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(RES_ID, container, false);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    protected void init() {

    }

    @Override
    protected void initView() {
        place = (TextView) rootView.findViewById(R.id.select_alba_place_name);
        location = (TextView) rootView.findViewById(R.id.select_alba_place_location);
    }
}

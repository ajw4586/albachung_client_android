package kr.albachung.stac.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.SparseIntArray;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import kr.albachung.stac.R;
import kr.albachung.stac.util.Logger;

/**
 * Created by globalsmartkr on 2015. 8. 11..
 */
public class SliderView extends View {


    public static final int LEVEL_POINT_AVERAGE = 50;

    public class ColorLevel {
        private SparseIntArray colors;

        public ColorLevel() {
            colors = new SparseIntArray();

            //  Preset color.
            colors.append(getLevelCount(), 0xFFEB6B0F);
            colors.append(getLevelCount(), 0XFFFAC747);
            colors.append(getLevelCount(), 0XFF79A5D6);
            colors.append(getLevelCount(), 0XFF26B5BA);
            colors.append(getLevelCount(), 0xFF8BC25B);

        }

        public int getLevelCount() {
            return colors.size();
        }

        public int getColor(int position) {
            return colors.get(position);
        }
    }

    //  Level starts 0.
    //  Default 1 consists of two level.
    private int maxLevel = 4;
    private float[] levelPoint = new float[maxLevel + 1];

    //  Level between level distance width.
    private float levelWidth = 0;

    //  current value how many the level is filled.
    private int curLevel = 1;

    private GestureDetector mGestureDetector;
    private TouchCallback touchCallbak;
    //  To transact touch swipe event position value.
    private float prevX = 0.0f, prevY = 0.0f;
    private float curX = 0.0f, curY = 0.0f;
    //  Slider drawing X Position
    private float sliderX = 0.0f;

    /**
     * Save the current line color.
     */
    private int curColor = 0x000000;
    private ColorLevel colorLevel;


    Paint mFillPaint, mBgPaint;
    Path bg_line, fill_line;

    class SliderAnimator extends Thread {

        float from, to, speed;
        int duration;

        public SliderAnimator(float from, float to, int duration) {
            this.from = from;
            this.to = to;
            this.duration = duration;
            this.speed = from - to / duration;
        }

        @Override
        public void run() {
            //  Animate until sanme or less than level point average.
            if(Math.abs(from - to) <= LEVEL_POINT_AVERAGE) {
                //  60 FPS
                try {   sleep(16);  } catch (InterruptedException e) {  e.printStackTrace();    }
                from += speed;
                setCurX(from);
                postInvalidate();
            }
        }
    }

    private long startAnimTime, elapsedAnimTime;

    public SliderView(Context context) {
        super(context);
        init();
    }

    public SliderView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAttrs(context, attrs);
        init();
    }

    public SliderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttrs(context, attrs);
        init();
    }

    public SliderView(Context context, int level) {
        this(context);
        if(level > 0)
            maxLevel = level;
        else
            Logger.except(getClass(), "level must bigger than 0");
    }

    protected void init() {
        touchCallbak = new TouchCallback();
        mGestureDetector = new GestureDetector(getContext(), touchCallbak);
        mFillPaint = new Paint();
        mBgPaint = new Paint();
        bg_line = new Path();
        fill_line = new Path();
        colorLevel = new ColorLevel();
    }

    protected void initAttrs(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.SliderView);

        final int idx = a.getIndexCount();
        for(int i = 0; i < idx; i++) {
            int attr = a.getIndex(i);
            switch (attr) {
                case R.styleable.SliderView_maxLevel:
                    setLevel(a.getInt(i, 0));
                    break;
                case R.styleable.SliderView_level:
                    setCurLevel(a.getInt(i, 0));
                    break;
            }
        }
        requestLayout();
        invalidate();
    }

    /**
     * Measure between the level-to-level width.
     * To implement magnetic function.
     */
    public void measureLevelWidth() {
        int width = getMeasuredWidth();
        if(width % getMaxLevel() > 0)
            setLevelWidth(width / getMaxLevel() + width % getMaxLevel());
        else
            setLevelWidth(width / getMaxLevel());
        for(int i = 0; i < getMaxLevel() + 1; i++) {
            levelPoint[i] = i * levelWidth;
            Logger.single(getClass(), "Level Point " + i + ": " + levelPoint[i]);
        }
    }

    public int getMaxLevel() {
        return maxLevel;
    }

    public void setLevel(int maxLevel) {
        this.maxLevel = maxLevel;
        this.levelPoint = new float[maxLevel + 1];
        requestLayout();
        invalidate();
    }

    public int getCurLevel() {
        return curLevel;
    }

    public void setCurLevel(int curLevel) {
        this.curLevel = curLevel;
        requestLayout();
        invalidate();
    }

    /**
     * Measure the drawing point of slider filled area.
     */
    public void measureSlider() {
        sliderX = curLevel * levelWidth;
    }

    public float getLevelWidth() {
        return levelWidth;
    }

    public void setLevelWidth(float levelWidth) {
        this.levelWidth = levelWidth;
    }

    public float getCurX() {
        return curX;
    }

    public void setCurX(float curX) {
        this.curX = curX;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        drawBG(canvas);
        drawToPercent(canvas);
    }

    /**
     * Draw Backgound of this view.
     * Always performed to draw rounded corners line.
     * @param canvas where we need to draw line.
     */
    public void drawBG(Canvas canvas) {
        mBgPaint.setColor(Color.LTGRAY);
        mBgPaint.setStyle(Paint.Style.STROKE);
        mBgPaint.setStrokeCap(Paint.Cap.ROUND);
        mBgPaint.setStrokeWidth(getHeight() * 0.75f);

        //  Move position to line start point.
        bg_line.moveTo(0 + getWidth() / 15, getHeight() / 2);
        bg_line.lineTo(getWidth() - getWidth() / 15,
                getHeight() / 2);

        canvas.drawPath(bg_line, mBgPaint);
    }

    public void drawToPercent(Canvas canvas) {
        Logger.multi("Slider Drawing", "Slider X Pos is " + sliderX);
        //  return if X position less than start point.
        if(getCurLevel() < 0 || getCurLevel() > getMaxLevel()) {
            return;
        }
        //  Set line color by Slider level.
        curColor = colorLevel.getColor(getCurLevel());
        mFillPaint.setColor(curColor);
        Logger.bold(getClass(), "cur color is " + curColor);
        mFillPaint.setStyle(Paint.Style.STROKE);
        mFillPaint.setStrokeCap(Paint.Cap.ROUND);
        mFillPaint.setStrokeWidth(getHeight() * 0.75f);

        //  Reset Path prevent to overdraw previous line.
        fill_line.reset();
        fill_line.moveTo(0 + getWidth() / 15, getHeight() / 2);

        if(sliderX <= 0) {
            fill_line.lineTo(getLevelWidth() / 15 + getWidth() / 15,
                    getHeight() / 2);
        } else {
            fill_line.lineTo(sliderX - getWidth() / 15,
                    getHeight() / 2);
        }
        canvas.drawPath(fill_line, mFillPaint);

    }

    /**
     * Set current level by current X position.
     * Automatically set level neal the X position level.
     */
    public void magnetic() {
        for(int level = 0; level < levelPoint.length; level++) {
            if (getCurX() <= levelPoint[level] + LEVEL_POINT_AVERAGE
                    && getCurX() >= levelPoint[level] - LEVEL_POINT_AVERAGE) {
                //  Get the original X position to use params of animateProgress().
                float originLevelPos = levelPoint[getCurLevel()];
                setCurLevel(level);
                Logger.single(getClass(), "level is set " + level);
                return;
            }
        }
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMeasureMode = MeasureSpec.getMode(widthMeasureSpec);
        int heightMeasureMode = MeasureSpec.getMode(heightMeasureSpec);
        int widthMeasureSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMeasureSize = MeasureSpec.getSize(heightMeasureSpec);

        if(widthMeasureMode != MeasureSpec.EXACTLY || heightMeasureMode != MeasureSpec.EXACTLY)
            throw new IllegalStateException("Width / Height must have an exact or MATCH_PARENT value");

        //  Measure the level width when view measure task performing.
        //  If level changed, requestLayout() must called to re-measure width.
        measureLevelWidth();
        measureSlider();
        setMeasuredDimension(widthMeasureSize, heightMeasureSize);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Logger.single(getClass(), "onTouch called");
        boolean result = mGestureDetector.onTouchEvent(event);

        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                break;
        }
        return result;
    }

    class TouchCallback extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            //  RIGHT to LEFT Scrolling
            if(prevX > e2.getX()) {
                Logger.multi("RIGHT to LEFT Scrolling", "e1 X: " + e1.getX(), "e2 X: " + e2.getX());
                curX = e2.getX();
                magnetic();
            }
            //  LEFT to RIGHT Scrolling
            if(e2.getX() > prevX) {
                Logger.multi("LEFT to RIGHT Scrolling", "e1 X: " + e1.getX(), "e2 X: " + e2.getX());
                curX = e2.getX();
                magnetic();
            }

            prevX = e2.getX();
            prevY = e2.getY();
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            return super.onFling(e1, e2, velocityX, velocityY);
        }

        @Override
        public boolean onDown(MotionEvent e) {
            prevX = curX = e.getX();
            prevY = curY = e.getY();
            return true;
        }
    }
}

package kr.albachung.stac.network.response;

/**
 * Created by Envy on 2015-06-26.
 */
public class AlbaDetailCommentsData extends CommentsData {

    private int like_count = 0;

    public AlbaDetailCommentsData(String name, String comments, String time, int like_count) {
        super(name, comments, time);
        this.like_count = like_count;
    }

    public void setData(String name, String comments, String time, int like_count) {
        super.setData(name, comments, time);
        this.like_count = like_count;
    }

    public int getLike_count() {
        return like_count;
    }

    public void setLike_count(int like_count) {
        this.like_count = like_count;
    }
}

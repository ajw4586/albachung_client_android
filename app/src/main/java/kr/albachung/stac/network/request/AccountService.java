package kr.albachung.stac.network.request;

import kr.albachung.stac.network.response.AccountResponse;
import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by globalsmartkr on 2015. 8. 27..
 */
public class AccountService {

    public interface AccountInterface {

        @FormUrlEncoded
        @POST("/login/facebook")
        void loginWithFacebook(@Field("token") String access_token,
                               @Field("user_id") String user_id,
                               Callback<AccountResponse> callback);
    }

    public static AccountInterface getInstance() {
        BaseService baseService = BaseService.getInstance();
        return baseService.getAlbachungAdapter().create(AccountInterface.class);
    }
}

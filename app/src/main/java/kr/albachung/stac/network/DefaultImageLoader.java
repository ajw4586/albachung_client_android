package kr.albachung.stac.network;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.LruCache;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by globalsmartkr on 2015. 9. 5..
 */
public class DefaultImageLoader {

    private static DefaultImageLoader instance;

    private static com.android.volley.toolbox.ImageLoader sImageLoader;
    private static RequestQueue sRequestQueue;

    public static DefaultImageLoader getInstance(Context context) {
        return instance != null ? instance : new DefaultImageLoader(context);
    }

    private DefaultImageLoader(Context context) {
        sRequestQueue = Volley.newRequestQueue(context);
        sImageLoader = new com.android.volley.toolbox.ImageLoader(sRequestQueue, new com.android.volley.toolbox.ImageLoader.ImageCache() {

            private LruCache<String, Bitmap> lruCache = new LruCache<String, Bitmap>(20);
            @Override
            public Bitmap getBitmap(String url) {
                return lruCache.get(url);
            }

            @Override
            public void putBitmap(String url, Bitmap bitmap) {
                lruCache.put(url, bitmap);
            }
        });
    }

    public RequestQueue getRequestQueue() {
        return sRequestQueue;
    }

    public com.android.volley.toolbox.ImageLoader getImageLoader() {
        return sImageLoader;
    }
}

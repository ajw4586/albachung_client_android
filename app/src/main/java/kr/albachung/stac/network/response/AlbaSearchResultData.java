package kr.albachung.stac.network.response;

import java.util.List;

/**
 * Created by globalsmartkr on 2015. 8. 15..
 */
public class AlbaSearchResultData extends BaseResponse {

    public List<AlbaInfo> info;


    public class AlbaInfo {
        public String _id;
        public String name;
        public String sub_name;
        public String category;
        public String address;
        public String new_address;
        public String contact;
        public float lat;
        public float lng;
    }
}

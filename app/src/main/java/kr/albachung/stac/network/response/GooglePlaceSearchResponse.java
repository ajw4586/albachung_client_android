package kr.albachung.stac.network.response;

import java.util.List;

/**
 * Created by globalsmartkr on 2015. 8. 25..
 */
public class GooglePlaceSearchResponse {

    public String status;
    public List<Result> results;
    public String next_page_token;

    public class Result {
        public Geometry geometry;
        public String icon;
        public String id;
        public String name;
        public String place_id;
        public String vicinity;
        public List<String> types;

        public class Geometry {
            public Location location;

            public class Location {
                public double lat;
                public double lng;
            }
        }
    }
}

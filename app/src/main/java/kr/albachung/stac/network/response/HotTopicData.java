package kr.albachung.stac.network.response;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import kr.albachung.stac.R;

/**
 * Created by Envy on 2015-06-21.
 */
public class HotTopicData extends TopicData {

    private boolean noti;

    public HotTopicData(String title) {
        super(title);
    }

    public HotTopicData(String title, String tag) {
        super(title, tag);
    }

    public boolean isNotification() {
        return noti;
    }

    @Override
    public void recognizeTag() {
        if(getTag().equals("noti"))
            noti = true;
        else
            noti = false;
    }

    public static class ViewHolder extends TopicData.ViewHolder {
        public TextView title;

        public ViewHolder(View v) {
            super(v);
        }


        @Override
        public void initView() {
            title = (TextView) parent.findViewById(R.id.item_post_list_title);
        }


    }

}

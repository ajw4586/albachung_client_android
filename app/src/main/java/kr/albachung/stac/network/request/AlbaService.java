package kr.albachung.stac.network.request;

import org.json.JSONObject;

import kr.albachung.stac.network.response.AlbaSearchResultData;
import kr.albachung.stac.network.response.BaseResponse;
import kr.albachung.stac.network.response.NoticeListsData;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by globalsmartkr on 2015. 8. 16..
 */
public class AlbaService {

    public interface AlbaInterface {

        //  Get review around your location.
        //  Based on latitude and longitude position.
        @GET("/alba/reviews")
        void getReview(@Query("access_token") String access_token,
                       @Query("lat") String lat,
                       @Query("lng") String lng,
                       Callback<AlbaSearchResultData> callback);

        //  Get review by your current location's address.
        //  This will search reviews within 군/구.
        @GET("/alba/reviews")
        void getReview(@Query("access_token") String access_token,
                       @Query("address") String address,
                       Callback<AlbaSearchResultData> callback);

        @POST("/alba/review")
        void postAlbaReview(@Body JSONObject request,
                            Callback<BaseResponse> callback);

        @GET("/notice/list")
        void getNoticeLists(Callback<NoticeListsData> callback);

    }

    public static AlbaInterface getInstance() {
        BaseService baseService = BaseService.getInstance();
        return baseService.getAlbachungAdapter().create(AlbaInterface.class);
    }
}

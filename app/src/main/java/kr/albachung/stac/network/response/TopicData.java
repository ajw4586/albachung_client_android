package kr.albachung.stac.network.response;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Envy on 2015-06-21.
 */
public class TopicData extends BaseResponse {

    private String title;
    private String tag;

    public TopicData(String title) {
        this.title = title;
    }

    public TopicData(String title, String tag) {
        this.title = title;
        this.tag = tag;
        recognizeTag();
    }

    public String getTitle() {
        return title;
    }

    public String getTag() {
        return tag;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public void setTag(String tag) {
        this.tag = tag;
        recognizeTag();
    }

    public void recognizeTag() {

    }

    public abstract static class ViewHolder extends RecyclerView.ViewHolder {

        protected View parent;


        public ViewHolder(View v) {
            super(v);
            parent = v;
            initView();
        }

        
        public abstract void initView();
    }
}

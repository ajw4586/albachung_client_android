package kr.albachung.stac.network.response;

/**
 * Created by Envy on 2015-06-26.
 */
public class CommentsData extends BaseResponse {

    private String name, comments, time;

    public CommentsData(String name, String comments, String time) {
        this.name = name;
        this.comments = comments;
        this.time = time;
    }

    public void setData(String name, String comments, String time) {
        this.name = name;
        this.comments = comments;
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}

package kr.albachung.stac.network.response;

import java.util.List;

/**
 * Created by globalsmartkr on 2015. 8. 30..
 */
public class GoogleDetailSearchResponse {

    public String status;
    public DetailResult result;

    public class DetailResult {
        public String id;
        public String formatted_phone_number;
        public String place_id;
        public List<String> types;
        public String url;
        public String website;
    }
}

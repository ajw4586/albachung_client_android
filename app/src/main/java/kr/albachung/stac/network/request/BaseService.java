package kr.albachung.stac.network.request;

import retrofit.Endpoint;
import retrofit.RestAdapter;

/**
 * Created by globalsmartkr on 2015. 8. 15..
 */
public class BaseService {

    public static final String ALBACHUNG_API = "http://175.158.15.182";
    public static final String GOOGLE_PLACES_SEARCH_API = "https://maps.googleapis.com/maps/api";

    //  private static MultiEndpoint ENDPOINT;
    private static BaseService instance = null;
    private RestAdapter albachungAdapter;
    private RestAdapter googleAdapter;

    protected BaseService() {
        //  ENDPOINT = new MultiEndpoint();
        //  ENDPOINT.setUrl(ALBACHUNG_API);
        albachungAdapter = new RestAdapter.Builder()
                .setEndpoint(ALBACHUNG_API)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        googleAdapter = new RestAdapter.Builder()
                .setEndpoint(GOOGLE_PLACES_SEARCH_API)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
    }

    public static BaseService getInstance() {
        return instance != null ? instance : new BaseService();
    }

    public RestAdapter getAlbachungAdapter() {
        return albachungAdapter;
    }

    public RestAdapter getGoogleAdapter() {
        return googleAdapter;
    }

    public class MultiEndpoint implements Endpoint {

        public String url;

        public void setUrl(String baseUrl) {
            url = baseUrl;
        }

        @Override
        public String getUrl() {
            if(url == null)
                throw new IllegalStateException("Base URL not set.");
            return url;
        }

        @Override
        public String getName() {
            return "default";
        }
    }

}

package kr.albachung.stac.network.response;

/**
 * Created by globalsmartkr on 2015. 8. 27..
 */
public class AccountResponse extends BaseResponse {

    public String access_token;
    public String expires_at;
    public User user;

    public class User {
        public String username;
        public String profile_img;
    }
}

package kr.albachung.stac.network.request;

import android.support.annotation.Nullable;

import kr.albachung.stac.network.response.GoogleDetailSearchResponse;
import kr.albachung.stac.network.response.GooglePlaceSearchResponse;
import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by globalsmartkr on 2015. 8. 25..
 */
public class GoogleService {

    public static final String APPKEY = "AIzaSyDKT5_LaceJtVDeZ83iddYDwyD4dx8OOzE";
    public static final String LANGUAGE = "ko";
    public static final String RADIUS = "3000";

    public interface GoogleInterface {

        /**
         * Search near places around your location.
         *
         * @param location
         * @param keyword
         * @param range
         * @param callback
         */
        @GET("/place/nearbysearch/json?key=" + APPKEY + "&language=ko")
        void searchByName(//    lat,lng 문자열로 전달
                          @Query("location") String location,
                          //    검색어 입력
                          @Query("keyword") String keyword,
                          //   검색 범위(단위: m)
                          @Query("radius") int range,
                          Callback<GooglePlaceSearchResponse> callback);

        /**
         * Search Place detail information.
         * You can receive quite detail about place.
         * The most important information are
         * phone number and google plus link, website url, etc.
         *
         * @param key
         * @param place_id
         * @param callback
         */
        @GET("/place/details/json")
        void searchDetail(@Query("key") String key,
                          //    장소의 고유 ID
                          @Query("placeid") String place_id,
                          Callback<GoogleDetailSearchResponse> callback);

        @GET("/staticmap?key=" + APPKEY + "&format=jpg&language=ko")
        void getMapImage(@Query("center") String centerPos,
                         @Query("zoom") int zoomLevel,
                         @Query("size") String size);
    }

    public static String createGoogleMapImageUrl(String latlng, String size, int zoomLevel) {
        return BaseService.GOOGLE_PLACES_SEARCH_API + "/staticmap?key=" + APPKEY +
                "&center=" + latlng +
                "&size=" + size +
                "&zoom=" + zoomLevel +
                "&markers=color:blue|" + latlng +
                "&language=ko";
    }

    public static GoogleInterface getInstance() {
        BaseService baseService = BaseService.getInstance();
        RequestInterceptor interceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addQueryParam("key", APPKEY);
                request.addQueryParam("language", LANGUAGE);
                request.addQueryParam("radius", RADIUS);
            }
        };
        return baseService.getGoogleAdapter().create(GoogleInterface.class);
    }

    public static String makeMapSize(float width, float height) {
        return (int) width + "x" + (int) height;
    }
}

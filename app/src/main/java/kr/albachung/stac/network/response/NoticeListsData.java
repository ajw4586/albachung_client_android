package kr.albachung.stac.network.response;

import java.util.List;

/**
 * Created by globalsmartkr on 2015. 9. 20..
 */
public class NoticeListsData {
    public List<Notice> data;

    public class Notice {
        public String title, url;
    }
}

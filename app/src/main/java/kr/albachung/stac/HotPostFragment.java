package kr.albachung.stac;


import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import kr.albachung.stac.adapter.HotTopicAdapter;
import kr.albachung.stac.network.request.AlbaService;
import kr.albachung.stac.network.response.NoticeListsData;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class HotPostFragment extends CommonFragment {


    protected static final int RES_ID = R.layout.fragment_hot_post;

    private RecyclerView postList;
    private LinearLayoutManager layoutManager;

    private HotTopicAdapter mainListAdapter;

    private AlbaService.AlbaInterface albaInterface = AlbaService.getInstance();

    public HotPostFragment() {
        albaInterface.getNoticeLists(new Callback<NoticeListsData>() {
            @Override
            public void success(NoticeListsData noticeListsData, Response response) {
                if(response.getStatus() == 200) {
                    mainListAdapter.updateData(noticeListsData);
                } else {
                    Toast.makeText(getActivity(), "글 목록을 가져올 수 없습니다.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(RES_ID, container, false);
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    @Override
    protected void init() {
        mainListAdapter = new HotTopicAdapter(new RequestWebListener() {
            @Override
            public void request(String url, String post_name) {
                Intent i = new Intent(getActivity(), AlbaMainWebView.class);
                i.putExtra("requestUrl", url);
                i.putExtra("postName", post_name);
                startActivity(i);
            }
        });
        layoutManager = new LinearLayoutManager(getActivity());
    }


    @Override
    protected void initView() {
        postList = (RecyclerView) rootView.findViewById(R.id.alba_main_list);
        postList.setAdapter(mainListAdapter);
        postList.setLayoutManager(layoutManager);
    }

    public interface RequestWebListener {
        void request(String url, String post_name);
    }

}

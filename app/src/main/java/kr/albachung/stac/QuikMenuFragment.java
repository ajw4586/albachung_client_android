package kr.albachung.stac;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import kr.albachung.stac.review.activity.ReviewPost;


/**
 * A placeholder fragment containing a simple view.
 */
public class QuikMenuFragment extends CommonFragment implements View.OnClickListener {


    protected static final int RES_ID = R.layout.fragment_quik_menu;

    private ImageButton quikPost;

    public QuikMenuFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(RES_ID, container, false);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    protected void initView() {
        quikPost = (ImageButton) rootView.findViewById(R.id.alba_main_icon);
        quikPost.setOnClickListener(this);
    }

    @Override
    protected void init() {

    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent();

        switch (v.getId()) {
            case R.id.alba_main_icon:
                intent.setClass(getActivity(), ReviewPost.class);
                getActivity().startActivity(intent);
                break;
        }
    }
}

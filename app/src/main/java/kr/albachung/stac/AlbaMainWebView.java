package kr.albachung.stac;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import kr.albachung.stac.util.Logger;

public class AlbaMainWebView extends CommonActivity {


    public static final int RES_ID = R.layout.activity_alba_main_web_view;


    private Toolbar toolbar;

    private AlbaMainWebViewFragment webViewFragment;

    private String currentUrl = "";
    private String title = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setLayoutRes(RES_ID);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void init() {
        Intent i = getIntent();
        currentUrl = i.getStringExtra("requestUrl");
        title = i.getStringExtra("postName");
        webViewFragment = new AlbaMainWebViewFragment();
    }

    @Override
    protected void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(title);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment, webViewFragment)
                .commit();
    }

    @Override
    protected void initMenu() {

    }


    public String getCurrentUrl() {
        return currentUrl;
    }

}

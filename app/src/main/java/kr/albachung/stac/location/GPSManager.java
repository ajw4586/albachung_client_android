package kr.albachung.stac.location;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import kr.albachung.stac.util.Logger;

/**
 * Created by Envy on 2015-06-21.
 */
public class GPSManager
        implements GoogleApiClient.ConnectionCallbacks
        ,GoogleApiClient.OnConnectionFailedListener
        ,LocationListener {

    private static GPSManager instance;

    private Context mContext;
    //  Last updated location.
    private Location lastLocation;
    private LocationResponse response;

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest locationRequest;


    private PendingResult<Status> pendingResult;

    public static GPSManager getInstance(Context context) {
        return instance == null ? new GPSManager(context) : instance;
    }

    private GPSManager(Context context) {
        mContext = context;
        instance = this;
    }

    public void setLocationResponse(LocationResponse response) {
        this.response = response;
    }

    public void getLocation() {
        if(mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            mGoogleApiClient.connect();
        } else {
            if(mGoogleApiClient.isConnected()) {
                lastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                if (lastLocation != null) {
                    Toast.makeText(mContext, "위치를 확인했습니다", Toast.LENGTH_SHORT).show();
                    response.onReceive(lastLocation);
                } else {
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                            setUpLocationRequest(), this);
                }
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        response.onReceive(location);
        Logger.single(getClass(), "위치 업데이트됨.");
    }

    LocationRequest setUpLocationRequest() {
        if(locationRequest != null) {
            return locationRequest;
        }
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        locationRequest.setNumUpdates(1);
        locationRequest.setFastestInterval(10000);
        locationRequest.setInterval(10000);
        locationRequest.setSmallestDisplacement(10);
        return locationRequest;
    }

    /**
    public static GPSManager getInstance(Context context, LocationResponse callback) {
        sContext = context;
        return instance == null
                ? new GPSManager(context).setCallback(callback)
                : instance.setCallback(callback);
    }
     */


    /**
     * Use this method only getting instance.
     * @param callback Response Interface.
     * @return Class instance.
     */
    /**
    GPSManager setCallback(LocationResponse callback) {
        mResponse = callback;
        return instance;
    }
     */

    /**
    public Location getLastUpdatedLocation() {
        return lastLocation;
    }
     */


    /**
    public void getLocation() {
        if(gpsEnabled) {
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 0, listener);
        } else if(networkEnable) {
            mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 2000, 0, listener);
        } else {
            Toast.makeText(sContext, "현재 위치를 가져올 수 없습니다.", Toast.LENGTH_LONG).show();
        }
    }

    void checkProviderAvailable() {
        try {
            gpsEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception e) {}
        try {
            networkEnable = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception e) {}
    }
     */

    /**
     * Listen location update in this interface.
     * Callback to LocationResponse Interface.
     */
    /**
    LocationListener listener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            lastLocation = location;
            mResponse.onReceive(lastLocation);
            mLocationManager.removeUpdates(listener);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };
     */


    public interface LocationResponse {
        public void onReceive(Location location);
    }

    @Override
    public void onConnected(Bundle bundle) {
        pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                setUpLocationRequest(), this);
        Toast.makeText(mContext, "현재 위치를 검색합니다.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(mContext, "Suspended to connect Google API Client", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Toast.makeText(mContext, "Failed to connect Google API Client" , Toast.LENGTH_SHORT).show();
        Logger.except(getClass(), connectionResult.toString());
    }
}

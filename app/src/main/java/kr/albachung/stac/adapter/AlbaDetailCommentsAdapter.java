package kr.albachung.stac.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import kr.albachung.stac.R;
import kr.albachung.stac.network.response.AlbaDetailCommentsData;
import kr.albachung.stac.network.response.CommentsData;

/**
 * Created by Envy on 2015-06-25.
 */
public class AlbaDetailCommentsAdapter extends BaseAdapter {

    private Context mContext;
    private ViewHolder holder;

    private ArrayList<AlbaDetailCommentsData> commentsData = new ArrayList<AlbaDetailCommentsData>();

    public AlbaDetailCommentsAdapter(Context context) {
        mContext = context;
        init();
    }


    protected void init() {
        commentsData.add(new AlbaDetailCommentsData("안재욱", "시급 많고, 다들 너무 친절하셨어요.", "2015-6-25", 5));
        commentsData.add(new AlbaDetailCommentsData("이영민", "시급 많고, 다들 너무 친절하셨어요.", "2015-6-23", 5));
        commentsData.add(new AlbaDetailCommentsData("안수민", "시급 많고, 다들 너무 친절하셨어요.", "2015-6-21", 5));
        commentsData.add(new AlbaDetailCommentsData("우인혜", "시급 많고, 다들 너무 친절하셨어요.", "2015-6-16", 5));
        commentsData.add(new AlbaDetailCommentsData("전현성", "시급 많고, 다들 너무 친절하셨어요.", "2015-6-13", 5));
        commentsData.add(new AlbaDetailCommentsData("우지우", "시급 많고, 다들 너무 친절하셨어요.", "2015-6-13", 5));
    }

    @Override
    public int getCount() {
        return commentsData.size();
    }

    @Override
    public Object getItem(int position) {
        return commentsData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_alba_detail_comments, null, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }
        holder = (ViewHolder) convertView.getTag();
        //  Setting up each row's data.
        holder.getName().setText(((CommentsData) getItem(position)).getName());
        holder.getTime().setText(((CommentsData) getItem(position)).getTime());
        holder.getComments().setText(((CommentsData) getItem(position)).getComments());

        return convertView;
    }


    class ViewHolder {
        private ImageView profile_image;
        private TextView name, comments, time;

        private View parent;

        ViewHolder(View v) {
            if(v == null) {
                throw new IllegalStateException("Parent View must be initialized before creating the ViewHolder.");
            }
            parent = v;
            init();
        }

        protected void init() {
            profile_image = (ImageView) parent.findViewById(R.id.item_alba_detail_comments_image);
            name = (TextView) parent.findViewById(R.id.item_alba_detail_comments_name);
            comments = (TextView) parent.findViewById(R.id.item_alba_detail_comments_comments);
            time = (TextView) parent.findViewById(R.id.item_alba_detail_comments_time);
        }

        public ImageView getProfile_image() {
            return profile_image;
        }

        public void setProfile_image(ImageView profile_image) {
            this.profile_image = profile_image;
        }

        public TextView getName() {
            return name;
        }

        public void setName(TextView name) {
            this.name = name;
        }

        public TextView getComments() {
            return comments;
        }

        public void setComments(TextView comments) {
            this.comments = comments;
        }

        public TextView getTime() {
            return time;
        }

        public void setTime(TextView time) {
            this.time = time;
        }

        public View getParent() {
            return parent;
        }

        public void setParent(View parent) {
            this.parent = parent;
        }
    }

}

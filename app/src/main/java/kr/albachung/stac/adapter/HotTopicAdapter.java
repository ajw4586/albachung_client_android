package kr.albachung.stac.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

import kr.albachung.stac.HotPostFragment;
import kr.albachung.stac.R;
import kr.albachung.stac.network.response.HotTopicData;
import kr.albachung.stac.network.response.NoticeListsData;
import kr.albachung.stac.util.Logger;

/**
 * Created by Envy on 2015-06-21.
 */
public class HotTopicAdapter extends RecyclerView.Adapter<HotTopicData.ViewHolder> {


    private NoticeListsData data;
    private HotPostFragment.RequestWebListener request;

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            NoticeListsData.Notice notice = data.data.get(((Integer) v.getTag()).intValue());
            request.request(notice.url, notice.title);
        }
    };


    public HotTopicAdapter(HotPostFragment.RequestWebListener listener) {
        request = listener;
    }

    public void updateData(NoticeListsData data) {
        this.data = data;
    }

    @Override
    public HotTopicData.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.item_post_list, null);
        itemView.setOnClickListener(clickListener);
        HotTopicData.ViewHolder holder = new HotTopicData.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(HotTopicData.ViewHolder holder, int position) {
        holder.itemView.setTag(new Integer(position));
        holder.title.setText(data.data.get(position).title);
    }

    @Override
    public int getItemCount() {
        return data != null ? data.data.size() : 0;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

}

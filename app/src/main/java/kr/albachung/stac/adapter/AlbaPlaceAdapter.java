package kr.albachung.stac.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import kr.albachung.stac.review.activity.AlbaPlaceDetail;
import kr.albachung.stac.R;

/**
 * Created by Envy on 2015-06-21.
 */
public class AlbaPlaceAdapter extends RecyclerView.Adapter {

    private ArrayList<AlbaPlaceData> data_list = new ArrayList<AlbaPlaceData>();

    public AlbaPlaceAdapter() {
        initData();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View albaPlaceView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_alba_place_info,viewGroup, false);
        return new ViewHolder(albaPlaceView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        AlbaPlaceData data = data_list.get(i);

        ViewHolder holder = (ViewHolder) viewHolder;
        holder.getName().setText(data.getName());
        holder.getPlace().setText(data.getPlace());
        holder.getDescription().setText(data.getSimple_info());
        holder.getRate().setImageResource(determineRateResources(data.getRate()));
        holder.getParent().setTag(R.id.view_holder_key, holder);

        holder.getParent().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), AlbaPlaceDetail.class);
                intent.putExtra("name", ((ViewHolder) v.getTag(R.id.view_holder_key))
                        .getName().getText().toString());
                intent.putExtra("place", ((ViewHolder) v.getTag(R.id.view_holder_key))
                        .getPlace().getText().toString());
                v.getContext().startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return data_list.size();
    }

    protected void initData() {
        data_list.add(new AlbaPlaceData("맥도날드", "강남점", "패스트푸드 전문점, 홈서비스, 캐릭터", 4.5f));
        data_list.add(new AlbaPlaceData("맥도날드", "강남 2호점", "패스트푸드 전문점, 홈서비스, 캐릭터", 2.4f));
        data_list.add(new AlbaPlaceData("맥도날드", "역삼점", "패스트푸드 전문점, 홈서비스, 캐릭터", 3.0f));
    }


    /**
     * Return the given rate's number image path.
     * @param rate Rate what you want to get image path.
     * @return Path of image resources based on the R.java file.
     */
    public int determineRateResources(float rate) {
        int result = -1;

        switch (Float.toString(rate)) {
            case "1.0":
            case "1.1":
            case "1.2":
            case "1.3":
            case "1.4":
            case "1.5":
            case "1.6":
            case "1.7":
            case "1.8":
            case "1.9":
            case "2.0":
            case "2.1":
            case "2.2":
            case "2.3":
                break;
            case "2.4":
                result = R.drawable.alba_rate_2_4;
                break;
            case "3.0":
                result = R.drawable.alba_rate_3_0;
                break;
            case "4.5":
                result = R.drawable.alba_rate_4_5;
                break;
        }
        return result;
    }

    class AlbaPlaceData {
        private String name, place, simple_info;
        private float rate;

        public AlbaPlaceData(String name, String place, String simple_info, float rate) {
            this.name = name;
            this.place = place;
            this.simple_info = simple_info;
            this.rate = rate;
        }

        public void setInfo(String name, String place, String simple_info, float rate) {
            this.name = name;
            this.place = place;
            this.simple_info = simple_info;
            this.rate = rate;
        }

        public String getName() {
            return name;
        }

        public String getPlace() {
            return place;
        }

        public String getSimple_info() {
            return simple_info;
        }

        public float getRate() {
            return rate;
        }


        public void setName(String name) {
            this.name = name;
        }

        public void setPlace(String place) {
            this.place = place;
        }

        public void setSimple_info(String simple_info) {
            this.simple_info = simple_info;
        }

        public void setRate(float rate) {
            this.rate = rate;
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private View parent;

        private TextView name, place, description;
        private ImageView rate;

        ViewHolder(View v) {
            super(v);
            parent = v;
            initView();
        }

        private void initView() {
            name = (TextView) parent.findViewById(R.id.item_alba_place_info_name);
            place = (TextView) parent.findViewById(R.id.item_alba_place_info_place);
            description = (TextView) parent.findViewById(R.id.item_alba_place_info_description);
            rate = (ImageView) parent.findViewById(R.id.item_alba_place_info_rate);
        }


        public View getParent() {
            return parent;
        }

        public TextView getName() {
            return name;
        }

        public TextView getPlace() {
            return place;
        }

        public TextView getDescription() {
            return description;
        }

        public ImageView getRate() {
            return rate;
        }
    }
}

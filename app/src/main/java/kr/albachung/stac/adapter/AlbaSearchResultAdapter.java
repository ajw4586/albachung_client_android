package kr.albachung.stac.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import kr.albachung.stac.R;
import kr.albachung.stac.network.DefaultImageLoader;
import kr.albachung.stac.network.request.GoogleService;
import kr.albachung.stac.network.response.GooglePlaceSearchResponse;
import kr.albachung.stac.util.Logger;
import kr.albachung.stac.util.Utils;

/**
 * Created by globalsmartkr on 2015. 8. 15..
 */
public class AlbaSearchResultAdapter extends RecyclerView.Adapter<AlbaSearchResultAdapter.ViewHolder> {

    public static final int NOT_SELECTED = -1;

    private ImageLoader mapLoader;
    private Context mContext;

    //  Handle Click Event
    private View.OnClickListener clickListener;
    private ViewGroup selectedView = null;
    private int selectedViewPosition = NOT_SELECTED;

    //  Adapter dataSet.
    private GooglePlaceSearchResponse data;

    public AlbaSearchResultAdapter() {
        clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectItem(v, ((Integer) v.getTag()).intValue());
            }
        };
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_alba_search_result, parent, false);
        mContext = itemView.getContext();
        itemView.setOnClickListener(clickListener);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        //  To determine position when clicked.
        holder.itemView.setTag(new Integer(position));

        holder.name.setText(data.results.get(position).name);
//        holder.subname.setText(data.info.get(position).contact);
        holder.address.setText(data.results.get(position).vicinity);
//        holder.newAddress.setText(data.info.get(position).new_address);
        holder.showMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.mapImageView.getVisibility() == View.VISIBLE)
                    holder.mapImageView.setVisibility(View.GONE);
                else {
                    holder.mapImageView.setVisibility(View.VISIBLE);
                    holder.mapImageView.setImageUrl(GoogleService.createGoogleMapImageUrl(createLatLng(data.results.get(position)),
                                            GoogleService.makeMapSize(holder.itemView.getWidth() / 1.5f,
                                                    Utils.dpToPx(mContext, 160) / 1.5f), 17),
                            mapLoader);
                }
            }
        });
    }

    /**
     * Highlight current selected item row's bg.
     * @param position
     */
    void selectItem(View selected, int position) {

        View top, bottom;
        top = selected.findViewById(R.id.item_alba_search_result_top_container);
        bottom = selected.findViewById(R.id.item_alba_search_result_bottom_container);


        //  Check view is already selected to select only one view.
        //  If another view has been selected, change the state
        //  unselect previous view and select the view that user current clicked.
        if(selectedView == null && selectedViewPosition == NOT_SELECTED) {
            Logger.bold(getClass(), "Item " + position + " is selected.");
            //  Set selected state.
            selectedView = (ViewGroup)selected;
            selectedViewPosition = position;
            //  Highlight background color.
            top.setBackgroundResource(R.drawable.ltgray_top_round_rect_selected);
            bottom.setBackgroundResource(R.drawable.darkergray_bottom_round_rect_selected);
        } else if(position == selectedViewPosition) {
            Logger.bold(getClass(), "Unselect item " + position);
            //  Unselect current selected view.
            //  Set the view background to default color.
            top.setBackgroundResource(R.drawable.ltgray_top_round_rect);
            bottom.setBackgroundResource(R.drawable.darkergray_bottom_round_rect);
            selectedView = null;
            selectedViewPosition = NOT_SELECTED;
        } else {
            Logger.bold(getClass(), "Unselect position " + selectedViewPosition + " and select position " + position);
            //  Perform unselect already selected view.
            //  Unselect the view using saved select information
            //  in this adapter instance.
            selectedView.findViewById(R.id.item_alba_search_result_top_container)
                    .setBackgroundResource(R.drawable.ltgray_top_round_rect);
            selectedView.findViewById(R.id.item_alba_search_result_bottom_container)
                    .setBackgroundResource(R.drawable.darkergray_bottom_round_rect);

            //  Change selected view to given current view instance
            //  And highlight that.
            selectedView = (ViewGroup) selected;
            selectedViewPosition = position;
            top.setBackgroundResource(R.drawable.ltgray_top_round_rect_selected);
            bottom.setBackgroundResource(R.drawable.darkergray_bottom_round_rect_selected);
        }
    }

    String createLatLng(GooglePlaceSearchResponse.Result result) {
        return result.geometry.location.lat + "," + result.geometry.location.lng;
    }

    @Override
    public int getItemCount() {
        return data != null ? data.results.size() : 0;
    }

    public GooglePlaceSearchResponse.Result getItem(int position) {
        return data.results.get(position);
    }

    public GooglePlaceSearchResponse.Result getSelectedItem() {
        return selectedViewPosition != NOT_SELECTED
                ? data.results.get(selectedViewPosition)
                : null;
    }


    public void updateData(GooglePlaceSearchResponse data) {
        this.data = data;
        notifyDataSetChanged();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name;
        public TextView address;
        public NetworkImageView mapImageView;
        public Button showMap;

        public ViewHolder(View itemView) {
            super(itemView);
            init();
        }

        public void init() {
            name = (TextView) itemView.findViewById(R.id.item_alba_search_result_name);
            address = (TextView) itemView.findViewById(R.id.item_alba_search_result_address);
            showMap = (Button) itemView.findViewById(R.id.item_alba_search_result_show_map);
            mapImageView = (NetworkImageView) itemView.findViewById(R.id.item_alba_search_result_map);

            //  Initialize Google Map
            mapLoader = DefaultImageLoader
                    .getInstance(mContext)
                    .getImageLoader();
        }
    }
}

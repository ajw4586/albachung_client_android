package kr.albachung.stac;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import kr.albachung.stac.network.request.AccountService;
import kr.albachung.stac.network.response.AccountResponse;
import kr.albachung.stac.preference.AccountPref;
import kr.albachung.stac.util.Logger;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class AlbaLogin extends CommonActivity {

    //  Facebook
    CallbackManager fbCallbackManager;
    LoginButton fbLogin;

    //  Server Login API
    AccountService.AccountInterface accountInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setLayoutRes(R.layout.activity_alba_login);
        super.onCreate(savedInstanceState);
    }

    void checkLogin() {
        if(AccountPref.isLogined(getApplicationContext())) {
            startActivity(new Intent(this, AlbaMain.class));
            finish();
        }
    }

    @Override
    protected void init() {
        //  Check login state and perform login or launch main screen.
        checkLogin();
        //  Initialize Facebook SDK
        FacebookSdk.sdkInitialize(getApplicationContext());
        fbCallbackManager = CallbackManager.Factory.create();
        accountInterface = AccountService.getInstance();
    }

    @Override
    protected void initView() {
        fbLogin = (LoginButton) findViewById(R.id.fb_login);
        fbLogin.setReadPermissions("public_profile");
        fbLogin.registerCallback(fbCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessToken access_token = loginResult.getAccessToken();
                String token = access_token.getToken();
                String user_id = access_token.getUserId();
                //  save Facebook user id and token data.
                AccountPref.saveFacebookUserData(AlbaLogin.this, token, user_id);
                Logger.bold(AlbaLogin.class, "FB Token: " + token + " user_id: " + user_id);
                accountInterface.loginWithFacebook(token, user_id, new Callback<AccountResponse>() {
                    @Override
                    public void success(AccountResponse accountResponse, Response response) {
                        Logger.bold(AlbaLogin.class, "Succeeded to login albachung.");
                        //  save albachung access token data.
                        AccountPref.saveAppUserInfo(AlbaLogin.this, accountResponse);
                        startActivity(new Intent(AlbaLogin.this, AlbaMain.class));
                        finish();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Logger.except(AlbaLogin.class, error.toString());
                    }
                });
            }

            @Override
            public void onCancel() {
                Toast.makeText(AlbaLogin.this, "CANCEL: to login facebook.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException e) {
                Toast.makeText(AlbaLogin.this, "ERROR: Failed to login facebook.", Toast.LENGTH_SHORT).show();
            }
        });
        fbLogin.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
        fbLogin.setBackgroundResource(R.drawable.fb_login_btn);
    }

    @Override
    protected void initMenu() {
    }

    /**
     * Hand to given activity result data
     * through fb callback manager.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        fbCallbackManager.onActivityResult(requestCode, resultCode, data);
    }
}

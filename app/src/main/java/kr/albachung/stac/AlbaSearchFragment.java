package kr.albachung.stac;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;


/**
 * A placeholder fragment containing a simple view.
 */
public class AlbaSearchFragment extends CommonFragment implements View.OnClickListener {


    protected static final int RES_ID = R.layout.fragment_alba_search;

    private EditText field;
    private ImageButton search, current_position;

    public AlbaSearchFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(RES_ID, container, false);
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    @Override
    protected void initView() {
        search = (ImageButton) rootView.findViewById(R.id.alba_search_button);
        current_position = (ImageButton) rootView.findViewById(R.id.alba_search_cur_pos);
        field = (EditText) rootView.findViewById(R.id.alba_search_field);
        search.setOnClickListener(this);
        current_position.setOnClickListener(this);
    }

    @Override
    protected void init() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.alba_search_button:
                Intent intent = new Intent(getActivity(), SearchResult.class);
                getActivity().startActivity(intent);
                break;
            case R.id.alba_search_cur_pos:
                break;
        }
    }
}

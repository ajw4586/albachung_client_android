package kr.albachung.stac;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class AlbaLocationPickerFragment extends CommonFragment {


    protected static final int RES_ID = R.layout.fragment_alba_location_picker;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(RES_ID, container, false);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    protected void init() {

    }

    @Override
    protected void initView() {

    }
}
